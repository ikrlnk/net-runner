//add this file to gitignore
const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  url: {
    workhost: process.env.WORKHOST,
    port: process.env.WORKHOST_PORT,
    loc: 'localhost'
  },
}
