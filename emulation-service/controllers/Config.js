const {pick, mockConfig} = require('../mock');
const Emulate = require('../wrapper');
const path = require('path');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
const { XMLParser, XMLBuilder, XMLValidator } = require('fast-xml-parser');

parser = new XMLParser({ 
  ignoreAttributes: false, 
  attributeNamePrefix: '' ,
});
const defaultPcapPath = (config) => path.resolve(__dirname, 'pcaps', `${config.id || config._id}_${Date.now()}.pcap`);

module.exports = {
  launch: async (config) => {
    const pcapPath = path.resolve(__dirname, '..', 'pcaps', uuidv4());
    fs.mkdirSync(pcapPath, { recursive: true });
    Emulate({
      config,
      pcapPath,
    });
    //console.log('root', path.basename(pcapPath));
    //console.log('emulation finished');
    const animeXML = fs.readFileSync(path.resolve(pcapPath, 'anime.xml')).toString();
    //console.log('anime', animeXML);
    const animeData = parser.parse(animeXML);
    return { root: path.basename(pcapPath), anime: animeData, pcaps: fs.readdirSync(pcapPath) }; 
  }
}
