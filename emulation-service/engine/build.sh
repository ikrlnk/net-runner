#!/bin/bash

root="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}"  )" &> /dev/null && pwd  )"
version="3.30"

download() {
  #git clone https://gitlab.com/nsnam/ns-3-allinone.git
  #cd ns-3-allinone
  #python download.py
  echo [*] starting download
  wget https://www.nsnam.org/release/ns-allinone-$version.tar.bz2 &> /dev/null;
  if [ $? -eq "0" ]; then
    echo [*] downloaded
  else
    echo [!] error downloading
    exit
  fi
  tar xjf ns-allinone-$version.tar.bz2
  echo [*] extracted
}

boost() {
  #TODO https://sourceforge.net/projects/boost/files/boost/1.38.0/boost_1_38_0.tar.gz/download -- download this version of boost
}

build() {
  echo [*] starting build, it takes time
  cd ns-allinone-$version
  fn="build.py"
  [[ -x $fn ]] && cmd="./$fn" || cmd="python $fn"
  echo [*] building with $cmd "&>" build.log
  $cmd &> ../build.log
  if [ $? -eq "0" ]; then
    echo [*] built
  else
    echo [*] error building, check build.log
    exit
  fi
}

install() {
  echo [*] linking to $root/NS
  ln -s $root/ns-allinone-$version/ns-$version $root/NS
}

echo [*] using version $version

#download
#build
install
