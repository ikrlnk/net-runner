const sample = {
  //id: 'da', 
  nodes: [
    {x: 0, y: 0, type: 'internet', devices: [{id: 'id1', ips: ['ip1']}]},
    {x: 200, y: 200, type: 'internet', devices: [{id: 'id2', ips: ['ip2']}]},
    {x: 400, y: 400, type: 'internet', devices: [{id: 'id3', ips: ['ip3']}, {id: 'id4', ips: ['ip4']}]},
    {x: 600, y: 600, type: 'internet', devices: [{id: 'id5', ips: ['ip5']}]},
  ],
  connections: [
    //{type: 'P2P', nodes: [0, 1], devices: ['id1', 'id2'], options: {}},
    {type: 'P2P', nodes: [2, 3], devices: ['id4', 'id5'], options: {}},
    {type: 'CSMA', nodes: [0, 1, 2], devices: ['id1', 'id2', 'id3'], options: {}},
  ],
  addrs: [
    {
      type: 'IP', 
      base: '10.0.1.0',
      mask: '255.255.255.0',
      devices: ['id1', 'id2', 'id3'],
      ips: ['ip1', 'ip2', 'ip3'],
    }, 
    {
      type: 'IP', 
      base: '10.0.2.0',
      mask: '255.255.255.0',
      devices: ['id4', 'id5'],
      ips: ['ip4', 'ip5'],
    }, 
  ],
  applications: [
    {type: 'udp-server', port: 8888, node: 0, start: 1.0, stop: 10.0},
    {type: 'udp-client', port: 8888, node: 3, dstIP: 'ip1', start: 2.0, stop: 10.0, options: {}},
  ],
  options: {
    populate: true, 
  }, 
  example: true, 
}
const pick = (obj, ...keys) => ({
  ...keys.reduce((res, e) => obj[e] !== undefined ? {...res, [e]:obj[e]} : {...res}, {})
})

module.exports = {mockConfig: sample, pick};
