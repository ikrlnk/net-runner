#!/usr/bin/env node

const { mockConfig } = require('./mock');
const configController = require('./controllers/Config');
const Config = require('./models/Config');

Config.updateOne({example: true}, {$set: {...mockConfig}}).then((res) => console.log(res));
