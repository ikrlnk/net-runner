const express           = require('express');
const log4js            = require('log4js');
const configController  = require('../controllers/Config'); 
const socket            = require('../socket');

const logger = log4js.getLogger('config');
logger.level = 'debug';
const router = express.Router();

router.post('/launch', async function(req, res, next) {
  try {
    console.log('data', JSON.stringify(req.body, null, 2));
    return res.json({
      message: 'ok',
      data: await configController.launch(req.body),
    })
  } catch(e) {
    logger.debug(e);
    return next(e);
  }
});

module.exports = router;
