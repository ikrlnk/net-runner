#!/bin/bash

#TODO version handling

root=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}"   )" &> /dev/null && pwd   );

path="$(pwd)/engine/ns-allinone-3.30/ns-3.30/build/lib"
echo [*] appending $path to LD_LIBRARY_PATH
if [ -n "$LD_LIBRARY_PATH" ] && [ -z "$(echo $LD_LIBRARY_PATH | grep $path)" ]; then
  LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$path
else
  LD_LIBRARY_PATH=$path
fi

export LD_LIBRARY_PATH

nodemon --watch $root node bin/www
