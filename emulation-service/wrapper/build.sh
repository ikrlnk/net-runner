#!/bin/bash

dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}"  )" &> /dev/null && pwd  )"
node_gyp_home="$HOME/.node-gyp" 

fix_rtti() {
  echo [*] assuming node-gyp install foler is $node_gyp_home
  if [ -d "$node_gyp_home" ]; then
    cd $node_gyp_home
    for i in $(grep -rn 'GCC_ENABLE_CPP_RTTI' | cut -d ':' -f 1); do
      sed -i "s/'GCC_ENABLE_CPP_RTTI': 'NO'/'GCC_ENABLE_CPP_RTTI': 'YES'/g" $i
      sed -i "s/'-fno-rtti',//g" $i;
      sed -i "s/'-fno-rtti'//g" $i;
    done
  else
    echo [!] it\'s not
  fi
} #NOT NEEDED, FIXED IN BINDING.GYP

lib_path() {
  path=$(realpath $dir/../engine/NS/build/lib/)
read -r -d '' LD << EOM
  if [ -n "\$LD_LIBRARY_PATH"  ]; then
      LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:$path
    else
      LD_LIBRARY_PATH=$path
  fi
  export LD_LIBRARY_PATH
EOM
  if [ -f "$HOME/.bashrc" ]; then
    while IFS= read -r line; do
      echo $line >> $HOME/.bashrc;
    done <<< "$LD"
  fi
  if [ -n "\$LD_LIBRARY_PATH"  ]; then
      LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:$path
    else
      LD_LIBRARY_PATH=$path
  fi
  export LD_LIBRARY_PATH
}

lib_path;
