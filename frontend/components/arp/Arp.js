import React, { useRef, useMemo, useState, useCallback, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { useTranslation } from 'next-i18next';

import styles from '@/assets/components/Arp.module.scss';
import classnames from 'classnames';

import Image from 'next/image';
import images from '@/lib/images';

import { useGraph } from '@/lib/context';

import {
  Grid,
  Card,
  Table,
  useModal,
  Modal,
  Collapse,
  Text,
  Badge,
  useTheme,
  Slider,
} from '@geist-ui/core';

function parseArp(cnt) {
  if (!cnt) return null;
  const res = [];
  for (const line of cnt.split('\n')) {
    if (line.includes('at time')) {
      const time = parseFloat(line.split('at time ')[1]);
      res.push({ time, recs: [] });
    }
    else if (line.length > 0) {
      const words = line.split(' ');
      const ip = words[0];
      const mac = words[words.length - 2];
      const state = words[words.length - 1];
      res[res.length - 1].recs.push({ ip, mac, state });
    }
  }
  return res;
}

export default function MyComponent({ children, className = '', namespace = 'common', ...props }) {
  const { t, i18n } = useTranslation(namespace, { keyPrefix: 'translation' });
  const { /*visible,*/ setVisible, bindings  } = useModal()
  const { arpData } = useGraph();
  useEffect(() => {
    if (arpData) {
      setVisible(true);
    }
  }, [arpData]);
  useEffect(() => {
    async function waitFor() {
      let el = document.querySelector('.dot');
      while (!el) {
        await new Promise((resolve, reject) => setTimeout(resolve, 10));
        el = document.querySelector('.dot');
      }
      el.style.pointerEvents = 'none';
    }
    const el = document.querySelector('.dot');
    if (!el || el.style.pointerEvents != 'none') {
      if (el) {
        el.style.pointerEvents = 'none';
      }
      else {
        waitFor();
      }
    }
  }, [arpData]);
  const data = useMemo(() => parseArp((arpData || {}).data), [arpData]);
  const [value, setValue] = useState(1);
  useEffect(() => {
    const el = document.querySelector('.dot');
    if (el) {
      el.textContent = `${value/100}`;
    }
  }, [value]);
  //console.log('arpData', arpData);
  if (!arpData) {
    //console.log('hiding');
    return (
      <span></span>
    );
  }
  return (
    <Modal {...bindings} wrapClassName={styles.pcap} width='80vw'>
      <Modal.Title>{t('arp-table')}</Modal.Title>
      <Modal.Subtitle>{t('arp-table-verbose')}</Modal.Subtitle>
      <Modal.Content>
        <Grid.Container alignItems='center' direction='column' gap={2} style={{width: '100%'}}>
          <Grid justify='center' alignItems='center' style={{width: '100%'}}>
            <Slider 
              step={1} 
              max={data[data.length - 1].time*100}
              min={1}
              value={value} 
              onChange={setValue} 
              showMarkers 
              hideValue
              width='75%' style={{margin: 'auto'}}/>
          </Grid>
          <Grid style={{width: '100%'}}>
            <Table data={data.find(e => e.time == value/100)?.recs || []} className={styles.pcapTable}>
              <Table.Column prop='ip' label='IP'/>
              <Table.Column prop='mac' label='MAC'/>
              <Table.Column prop='state' label='STATE'/>
            </Table>
          </Grid>
        </Grid.Container>
      </Modal.Content>
    </Modal>
  );
};
