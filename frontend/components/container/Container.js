import React, { useRef, useState, useCallback, useEffect } from 'react';
import styles from '@/assets/components/Container.module.scss';

export default function Container({ children, ...props }) {
  return (
    <div className={styles.container} {...props}>
      {children}
    </div>
  )
};
