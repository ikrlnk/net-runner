import React, { useRef, useState, useCallback, useEffect } from 'react';
import { useTranslation } from 'next-i18next';

import { useRouter } from 'next/router';

import styles from '@/assets/components/Header.module.scss';
import classnames from 'classnames';

import {
  useMain,
} from '@/lib/context';

import {
  Page,
  Grid,
  Text,
  Button,
  Tabs,
} from '@geist-ui/core';

import {
  Sun, Moon,
} from '@geist-ui/icons';

import Image from 'next/image';
import images from '@/lib/images';

export default function MyComponent({ children, className = '', namespace = 'common', ...props }) {
  const { t, i18n } = useTranslation(namespace, { keyPrefix: 'translation' });
  const { switchThemes, themeType } = useMain();
  const router = useRouter();
  const { pathname } = router;
  const goTo = useCallback((loc) => {
    router.push(loc);
  }, [router]);
  return (
    <Page.Header className={styles.header} {...props}>
      <Grid.Container gap={2} justify='conter'>
        <Grid xs={10} justify='center' alignItems='center'>
          <Image src={images.logo} width={48} height={48}/>
          <Text b>{t('name')}</Text>
        </Grid>
        <Grid xs={4} justify='center' alignItems='center'>
          <Tabs initialValue={pathname} hideDivider onChange={goTo}>
            <Tabs.Item label={t('header.main')} value='/'/>
            <Tabs.Item label={t('header.playground')} value='/playground'/>
            <Tabs.Item label={t('header.help')} value='/help'/>
          </Tabs>
        </Grid>
        <Grid xs={10} justify='center' alignItems='center'>
          <Button onClick={switchThemes} icon={themeType == 'light' ? <Sun/> : <Moon/>}>
            {themeType == 'light' ? t('light') : t('dark') }
          </Button>
        </Grid>
      </Grid.Container>
    </Page.Header>
  );
};
