import React, { useRef, useState, useCallback, useEffect } from 'react';
import styles from '@/assets/components/LandingBlock.module.scss';
import classnames from 'classnames';

const MyComponent = React.forwardRef((props, ref) => {
  const { className = null, children = null } = props;
  return (
    <section className={styles.landingBlock} ref={ref}>
      <div className={className || ''}>
        {children}
      </div>
    </section>
  );
});

MyComponent.displayName = 'LandingBlock';

export default MyComponent;
