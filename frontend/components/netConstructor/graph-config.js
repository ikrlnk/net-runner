import * as React from 'react';

export const NODE_KEY = 'id'; // Key used to identify nodes

// These keys are arbitrary (but must match the config)
// However, GraphView renders text differently for empty types
// so this has to be passed in if that behavior is desired.
export const NODE_TYPE = 'node'; // Empty node type
export const LAN_TYPE = 'lan';
export const PACKET_TYPE = 'packet';
export const EMPTY_EDGE_TYPE = 'emptyEdge';
export const P2P_EDGE_TYPE = 'p2pEdge';

export const nodeTypes = [NODE_TYPE, LAN_TYPE, PACKET_TYPE];
export const edgeTypes = [EMPTY_EDGE_TYPE, P2P_EDGE_TYPE];

export const NodeTypeTitles = {
  node: 'Node',
  lan: 'LAN', 
};

const NodeShape = (
  <symbol viewBox="0 0 92 92" width="92" height="92" id="emptyNode">
    <circle cx="46" cy="46" r="46" />
  </symbol>
);

const LANShape = (
  <symbol viewBox="0 0 92 92" width="92" height="92" id="lanNode">
    <circle cx="46" cy="46" r="46" />
  </symbol>
);

const PacketShape = (
  <symbol viewBox="0 0 24 24" width="24" height="24" id="packetNode" fill="pink">
    <circle cx="12" cy="12" r="12" />
  </symbol>
);

const EmptyEdgeShape = (
  <symbol viewBox="0 0 50 50" id="emptyEdge">
    {/*<circle cx="25" cy="25" r="8" fill="currentColor"/>*/}
  </symbol>
);

const P2PEdgeShape = (
  <symbol viewBox="0 0 50 50" id="p2pEdge">
    <rect
      transform="rotate(45)"
      x="27.5"
      y="-7.5"
      width="15"
      height="15"
      fill="#fff"
      stroke="#fff"
    />
  </symbol>
);

export default {
  EdgeTypes: {
    [EMPTY_EDGE_TYPE]: {
      shape: EmptyEdgeShape,
      //shapeId: '#emptyEdge',
      shapeId: '#' + EMPTY_EDGE_TYPE,
    },
    [P2P_EDGE_TYPE]: {
      shape: P2PEdgeShape,
      shapeId: '#' + P2P_EDGE_TYPE,
    },
  },
  NodeSubtypes: {

  },
  NodeTypes: {
    [NODE_TYPE]: {
      shape: NodeShape,
      shapeId: '#emptyNode',
      typeText: 'Node',
    },
    [LAN_TYPE]: {
      shape: LANShape,
      shapeId: '#lanNode',
      typeText: 'LAN',
    },
    [PACKET_TYPE]: {
      shape: PacketShape,
      shapeId: '#packetNode',
      typeText: 'packet',
    },
  },
};
