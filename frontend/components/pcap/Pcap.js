import React, { useRef, useState, useCallback, useMemo, useEffect } from 'react';
import { useTranslation } from 'next-i18next';

import styles from '@/assets/components/Pcap.module.scss';
import classnames from 'classnames';

import sampleDump from '@/lib/sampleDump';

import { useGraph } from '@/lib/context';

import {
  Grid,
  Card,
  Table,
  useModal,
  Modal,
  Collapse,
  Text,
  Badge,
  useTheme,
} from '@geist-ui/core';

function getProtocol(packet) {
  const { _source: { layers } } = packet;
  const { frame, ip } = layers;
  try {
    //return frame['frame.protocols'].split(':ip:')[1].split(':')[0].toUpperCase();
    const protos = frame['frame.protocols'].split(':');
    const res = (protos[protos.length - 1] == 'data' ? protos[protos.length - 2] : protos[protos.length - 1]).toUpperCase();
    //console.log('protos', protos, res);
    return res;
  } catch(e) {
    return '';
  }
}

function getInfo(packet) {
  const { _source: { layers } } = packet;
  if (getProtocol(packet) == 'UDP') {
    const { udp } = layers;
    return `${udp['udp.srcport']} -> ${udp['udp.dstport']}`;
  }
  return '';
}

function isPrint(c) {
  return !(/[\x00-\x08\x0E-\x1F\x80-\xFF]/.test(String.fromCharCode(c)));
}

function isAscii(c) {
  return (c & ~0x7f) == 0;
}

function parseRow(t, packet, namespace = 'common') {
  const { _source: { layers } } = packet;
  const { frame, ip, eth } = layers;
  const notImplemented = !frame || !ip;
  const source = ip ? ip['ip.src'] : eth ? eth['eth.src'] : t('not-implemented'); //TODO work with ether frames
  const destination = ip ? ip['ip.dst'] : eth ? eth['eth.dst'] : t('not-implemented');
  const protocol = getProtocol(packet) || t('not-implemented');
  return {
    no: frame['frame.number'],
    time: Number(frame['frame.time_relative']).toFixed(6),
    source, destination, protocol,
    length: frame['frame.len'],
    info: getInfo(packet),
    raw: packet,
  }
}

function HexDump({ namespace = 'common', ...props }) {
  const { t, i18n } = useTranslation(namespace, { keyPrefix: 'translation' });
  const theme = useTheme();
  const { palette } = theme;
  const { data, cols = 16, showAscii = true, className = '', ...otherProps  } = props;
  const [state, setState] = useState({ hStart: 0, hEnd: 0  });
  const nLen = 8;//TODO
  const sliced = useMemo(() => data.eachSlice(cols).map(slice => slice.concat(Array(16).fill(-1)).slice(0, 16)), [data]);
  console.log('sliced', sliced);
  const finalData = sliced.map((slice, i) => ({ 
    offset: (i * cols).toString(16).padStart(nLen, '0') ,
    val: slice.map(e => e == -1 ? ' ' : e.toString(16).padStart(2, '0')).map((e, i) => 
      <Badge style={{ backgroundColor: 'transparent', color: theme.type == 'dark' ? palette.cyan : palette.violet }} key={i}>{e.toUpperCase()}</Badge>
    ),
    ascii: slice.map(e => e == -1 ? ' ' : isPrint(e) && isAscii(e) ? String.fromCharCode(e) : '.').join(' '),
  }));
  return (
    <Table data={finalData} className={styles.hexDump}>
      <Table.Column prop='offset' label={t('hexdump')} className={styles.noSelect}/>
      <Table.Column prop='val' label=''/>
      <Table.Column prop='ascii' label='' className={styles.noSelect}/>
    </Table>
  )
}

function PacketInfo({ namespace = 'common', packet, ...props }) {
  const { _source: { layers  } } = packet.raw;
  const { data, icmp } = layers;
  console.log('layers', layers);
  const bytes = (data ? data['data.data'] : (icmp && icmp.data) ? icmp.data['data.data'] : '').split(':').map(e => parseInt(e, 16)).filter(e => !Number.isNaN(e));
  const textContent = useMemo(() => Object.values(layers).map(e => e.text).filter(e => e), [layers]);
  console.log('textContent', textContent);
  return (
    <Grid.Container direction='column' gap={1}>
      <Card>
        <Collapse.Group accordion={false}>
          {textContent.map((e, i) => (
            <Collapse title={e.title} key={i}>
              {e.content.map((e, j) => <Text key={j}>{e}</Text>)}
            </Collapse>
          ))}
        </Collapse.Group>
      </Card>
      <Card>
        <HexDump data={bytes}/>
      </Card>
    </Grid.Container>
  )
}

export default function MyComponent({ children, className = '', namespace = 'common', /*data,*/ visible = false, ...props }) {
  const { t, i18n } = useTranslation(namespace, { keyPrefix: 'translation' });
  const { pcapData } = useGraph();
  const data = pcapData || sampleDump;
  const [state, setState] = useState({ currentPacket: null });
  const inspectPacket = useCallback((packet) => {
    //console.log('packet', packet);
    setState(state => ({...state, currentPacket: packet}));
  }, [setState]);
  const { /*visible,*/ setVisible, bindings  } = useModal()
  useEffect(() => {
    //setVisible(visible);
    if (pcapData) {
      setVisible(true);
    }
  }, [pcapData]);
  return (
    <Modal {...bindings} wrapClassName={styles.pcap} width='80vw'>
      <Modal.Title>{t('pcap-dump')}</Modal.Title>
      <Modal.Subtitle>{t('pcap-dump-verbose')}</Modal.Subtitle>
      <Modal.Content>
        <Grid.Container gap={2}>
          <Grid xs={12}>
            <Table data={data.map(parseRow.bind(null, t))} onRow={inspectPacket} className={styles.pcapTable}>
              <Table.Column prop='no' label='no'/>
              <Table.Column prop='time' label='time'/>
              <Table.Column prop='source' label='source'/>
              <Table.Column prop='destination' label='destination'/>
              <Table.Column prop='protocol' label='protocol'/>
              <Table.Column prop='length' label='length'/>
              <Table.Column prop='info' label='info'/>
            </Table>
          </Grid>
          <Grid xs={12}>
            {state.currentPacket ? <PacketInfo packet={state.currentPacket}/> : <div/>}
          </Grid>
        </Grid.Container>
      </Modal.Content>
    </Modal>
  );
};
