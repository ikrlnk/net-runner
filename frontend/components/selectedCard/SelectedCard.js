import React, { useRef, useState, useCallback, useEffect } from 'react';
import { useTranslation } from 'next-i18next';

import styles from '@/assets/components/SelectedCard.module.scss';
import classnames from 'classnames';

import Image from 'next/image';
import images from '@/lib/images';

import { 
  useGraph,
} from '@/lib/context';

import {
  useTheme,
  Grid,
  Card,
  Text,
  Divider,
  Input,
  Spacer,
  Button,
  Select,
} from '@geist-ui/core';

import {
  Delete,
  PlusCircle,
} from '@geist-ui/icons';

import isHotkey from 'is-hotkey';

const ipRex = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
export default function SelectedCard ({ /*selected, graphRef,*/ onSubmit = e => e, namespace = 'common' }) {
  const { updateNode, removeNode, graph, selected, graphRef, stage, resp, openPcap, openArp = f => f } = useGraph();

  const cur = (selected ? (graph.nodes.find(e => e._id == selected._id) || {}) : {});
  const { type, applications = [], id } = cur;

  const [apps, setApps] = useState([]);
  useEffect(() => {
    setApps([]);
  }, [id]);

  const addApplication = useCallback((type) => {
    setApps(state => state.concat({ type, dst: '' }));
  });

  const removeApplication = useCallback((idx) => {
    const newNode = { ...cur, applications: applications.slice(0, idx).concat(applications.slice(idx + 1)) };
    //console.log('applications', applications);
    //console.log('new node', newNode);
    updateNode(newNode);
  }, [updateNode, cur]);

  const validBase = useCallback((val) => {
    //console.log('valid', val, ipRex.test(val));
    return ipRex.test(val);
  }, []);

  const updateApp = useCallback((setState, idx, evt) => {
    setState(state => {
      const newState = [...state];
      state[idx].dst = evt.target.value;
      return newState;
    });
  }, []);
  //const updateInput = useCallback((setState, evt) => setState(evt.target.value), []);
  const submitApplication = useCallback((idx) => {
    const newNode = { ...cur, applications: applications.concat(apps[idx]) };
    setApps(apps => apps.slice(0, idx).concat(apps.slice(idx + 1)));
    updateNode(newNode);
  }, [updateNode, cur, apps]);

  const catchEnter = useCallback((i, evt) => {
    if (isHotkey('Enter', evt)) {
      submitApplication(i);
    }
  }, [submitApplication]);

  const horBounds = useCallback((x, width) => {
    return Math.max(Math.min(x, Math.abs(width - 400)), 0);
  }, []);
  const verBounds = useCallback((y, height) => {
    return Math.max(Math.min(y, Math.abs(height - 600)), 0);
  }, []);

  if (!selected || stage == 1 || stage == 2) {
    return <span></span>;
  }
  const style = {position: 'absolute', width: 'max-content'};
  if (graphRef.current) {
    const { width, height } = graphRef.current.getBoundingClientRect();
    const { x, y } = selected;
    const g = document.querySelector(`#node-${id}-container`);
    if (g) {
      const rect = g.getBoundingClientRect();
      const { x, y } = rect;
      style.left = horBounds(x, width); 
      style.top = verBounds(y, height);
      //console.log('rect', g, rect);
    }
    else {
      //console.log('graphref', { x, y }, { width, height });
      //style.left = Math.max(Math.min(x, Math.abs(width - 200)), -200);
      //style.top = Math.max(Math.min(y, Math.abs(height - 300)), 0);
      style.left = horBounds(x, width); 
      style.top = verBounds(y, height);
    }
  }
  else {
    style.left = selected.x;
    style.top = selected.y;
  }
  const IPs = graph.edges.filter(e => e.source == id || e.target == id).map(e => e.source == id ? e.sourceIP : e.targetIP);
  const value = {
    resp, graph, selected, type, applications, id, 
    apps, setApps, addApplication, removeApplication, 
    style, cur, validBase, updateApp, submitApplication, 
    openPcap, IPs, catchEnter, openArp,
  };

  if (stage == 3) {
    return <PcapCard {...value}/>
  }
  if (type == 'pc') {
    return <CompCard {...value}/>;
  }
  else if (type == 'switch') {
    return <SwitchCard {...value}/>;
  }
  else if (type == 'hub') {
    return <HubCard {...value}/>
  }
}

export function PcapCard({ namespace = 'common', ...props }) {
  const { t, i18n } = useTranslation(namespace, { keyPrefix: 'translation' });
  const { style, openPcap, resp, cur, openArp, type } = props;
  return (
    <Card className={styles.selectedCard} style={style}>
      <Text h4 my={0} align='center'>{t('pcaps')}</Text>
      <Spacer h={1}/>
      <Divider align='center' style={{ whiteSpace: 'nowrap' }}>{t('available-files')}</Divider> 
      {resp.data.pcaps.filter(e => parseInt(e) == cur.id && e.includes('.pcap')).map((e, i) => (
        <Text key={i} style={{textAlign: 'center', marginTop: '0', marginBottom: '0', cursor: 'pointer'}} onClick={openPcap.bind(null, e)}>{e}</Text>
      ))}
      {type == 'pc' &&
        <>
        <Divider align='center' style={{ whiteSpace: 'nowrap' }}>{t('arp-table')}</Divider> 
        {resp.data.pcaps.filter(e => parseInt(e) == cur.id && e.includes('arp')).map((e, i) => (
          <Text key={i} style={{textAlign: 'center', marginTop: '0', marginBottom: '0', cursor: 'pointer'}} onClick={openArp.bind(null, e)}>{e}</Text>
        ))}
        </>
      }
    </Card>
  );
}

export function CompCard({ namespace = 'common', ...props }) {
  const theme = useTheme();
  const { t, i18n } = useTranslation(namespace, { keyPrefix: 'translation' });
  const { catchEnter, style, selected, applications, removeApplication, apps, setApps, updateApp, submitApplication, addApplication, graph, id, IPs } = props;
  return (
    <Card className={styles.selectedCard} style={style}>
      <Text h4 my={0} align='center'>{selected.type.toUpperCase() +  ' ' + t('device-settings')}</Text>
      <Spacer h={1}/>
      <Divider>{t('services')}</Divider> 
      <Spacer h={2}/>
      {applications.map((e, i) => (
        <Grid.Container key={i} alignItems='center' gap={1} justify='space-between'>
          {/*<Grid>
            <Text small>{e.type}</Text>
          </Grid>*/}
          <Grid style={{flex: '1'}}>
            <Input label={e.type} value={e.type != 'sink' ? e.dst : 'Any IP'} disabled width='100%'/>
          </Grid>
          <Grid alignItems='center'>
            <Delete color={theme.type == 'dark' ? '#fff' : '#000'} className={styles.pointer} onClick={removeApplication.bind(null, i)}/>
          </Grid>
        </Grid.Container>
      ))}
      {apps.map((e, i) => (
        <Grid.Container key={i} alignItems='center' gap={1} justify='space-between'>
          {/*<Grid>
            <Text small>{e.type}</Text>
          </Grid>*/}
          <Grid style={{ flex: '1' }}>
            <Input 
              width='100%'
              label={e.type}
              onKeyDown={catchEnter.bind(null, i)}
              value={e.type != 'sink' ? e.dst : 'Any IP'}
              disabled={e.type == 'sink'} 
              onChange={updateApp.bind(null, setApps, i)} 
              placeholder={e.type == 'ping' ? '192.168.1.3' : e.type.includes('client') ? '192.168.1.3:3000' : e.type.includes('server') ? '3000' : ''}/>
          </Grid>
          <Grid alignItems='center'>
            <PlusCircle color={theme.type == 'dark' ? '#fff' : '#000'} className={styles.pointer} onClick={submitApplication.bind(null, i)}/>
          </Grid>
        </Grid.Container>
      ))}
      <Spacer h={2}/>
      <Select placeholder={t('add-application')} style={{width: '100%'}} onChange={addApplication}>
        <Select.Option value={t('ping')}>{t('ping')}</Select.Option>
        <Select.Option value={t('sink')}>{t('sink')}</Select.Option>
        <Select.Option value={'udp-client'}>{t('udp-client')}</Select.Option>
        <Select.Option value={'udp-server'}>{t('udp-server')}</Select.Option>
        <Select.Option value={'tcp-client'}>{t('tcp-client')}</Select.Option>
        <Select.Option value={'tcp-server'}>{t('tcp-server')}</Select.Option>
      </Select>
      <Spacer h={2}/>
      <Divider>{t('ips')}</Divider> 
      {IPs.map((e, i) => (
        <Text key={i} style={{textAlign: 'center', marginTop: '0', marginBottom: '0'}}>{e}</Text>
      ))}
    </Card>
  );
}

export function HubCard(props) {
  const { style, selected } = props;
  return (
    <Card className={styles.selectedCard} style={style}>
      <Text h4 my={0} align='center'>{selected.type}</Text>
    </Card>
  );
}

export function SwitchCard(props) {
  const { style, selected } = props;
  return (
    <Card className={styles.selectedCard} style={style}>
      <Text h4 my={0} align='center'>{selected.type}</Text>
      {/*<Spacer h={1}/>
      <Divider>{t('ip-base')}</Divider>
      <Spacer h={2}/>
      <Input placeholder='192.168.1.0' value={IPBase} onChange={updateInput.bind(null, setIPBase)}/>
      <Spacer h={1}/>
      <Button disabled={!validBase(IPBase)} style={{width: '100%'}}>{t('apply')}</Button>*/}
    </Card>
  );
}
