#!/bin/bash

root=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}"  )" &> /dev/null && pwd  );
name=$1

if [ -z "$name" ]; then
  echo [!] specify imgs name 1>&2;
  exit 1;
fi

cd $root/public
for zipFile in *.zip; do
  if [ -z "tmp" ]; then
    rm -rf tmp
  fi
  unzip "$zipFile" -d tmp
  cd tmp
  i=1
  for file in *; do
    extension="${file##*.}"
    echo "[*] $file -> $name-$i.$extension"
    mv "$file" $name-$i.$extension
    i=$(($i+1))
  done
  cd ..
  mv tmp/* .
  rm -rf tmp
  rm "$zipFile"
  break; #files from figma usually have project's name, so working for sinle zip only
done

#TODO add to lib/images.js
#TODO move to new dir

echo [*] done
