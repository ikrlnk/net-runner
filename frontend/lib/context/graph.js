import React, { useRef, useState, useCallback, useEffect, createContext } from 'react';
import { useTranslation } from 'next-i18next';
import axios from 'axios';

import styles from '@/assets/Graph.module.scss'

import { useIPPool, useAnimation } from '@/lib/hooks';

import sampleDump from '@/lib/sampleDump';
import sampleGraph from '@/lib/sampleGraph';

import { v4 as uuidv4 } from 'uuid';

const GraphContext = createContext();

const dev = process.env.NODE_ENV !== 'production';

function nop(e) {
  return e;
}

export function Provider({ children, namespace = 'common' }) {
  const [stage, setStage] = useState(0);
  const [resp, setResp] = useState(null);

  const [state, setState] = useState({ selected: null });
  const [rawSelected, setSelected] = useState(null); //used for native react digraph

  const [options, setOptions] = useState({ 
    speed: 0.0015,
    speedLocal: '0.0015',
    animeLen: 0.5,
    animeLenLocal: '0.5',
    populateIP: false,
  });

  const [graph, setGraph] = useState(sampleGraph);

  const { getIP, IPPool, reserveIP } = useIPPool({ initGraph: sampleGraph });

  const stopAnimation = setStage.bind(null, 3);
  const animationNodes = useAnimation({
    active: (stage == 2 && resp),
    activeWrapper: { active: stage == 2 && resp }, 
    onFinish: stopAnimation,
    anime: resp?.data?.anime,
    options, graph,
  });

  const launch = useCallback(async () => {
    try {
      setStage(1);
      const base = dev ? process.env.NEXT_PUBLIC_EMULATION_API : process.env.NEXT_PUBLIC_EMULATION_API_PROD;
      const url = new URL('/config/launch', base) ;
      console.log('url', url);
      const data = await axios.post(url.href, {
        options,
        ...graph,
      }).then(resp => resp.data);
      //alert(JSON.stringify(data));
      console.log('data', data);
      setResp(data);
      setStage(2);
    } catch(e) {
      console.error('error', e.toString());
    }
  }, [options, graph, setStage, setResp]);

  const [keyPrefix, setKeyPrefix] = useState('0');
  const rerender = useCallback(() => {
    setKeyPrefix(key => (parseInt(key) + 1).toString(16));
  }, [keyPrefix, setKeyPrefix]);


  const newValidated = useCallback((state => ({...state, nodes: state.nodes.sort((a, b) => a.id - b.id)})));
  const addNode = useCallback((type) => {
    const { nodes, edges } = graph;
    setGraph(state => {
      const newState = newValidated(state);
      const lastNode = (newState.nodes[newState.nodes.length - 1] || { x: 0, y: 0, id: -1 });
      newState.nodes = [
        ...newState.nodes, 
        {
          type,
          _id: uuidv4(),
          id: lastNode.id + 1,
          x: lastNode.x + 100,
          y: lastNode.y + 100,
          applications: [],
        },
      ];
      return newState;
    });
  }, [graph, setGraph, rerender]);

  const removeEdge = useCallback((edge) => {
    setGraph(state => {
      const newState = newValidated(state);
      newState.edges = newState.edges.filter(e => !(e.source == edge.source && e.target == edge.target));
      return newState;
    })
  }, [setGraph]);

  const removeNode = useCallback((node) => {
    const { _id } = node;
    setGraph(state => {
      //console.log('init state', _id, state);
      const newState = newValidated(state);
      const idx = newState.nodes.findIndex(e => e._id == _id);
      if (idx == -1) return newState;
      //console.log('idx', idx);
      const newNodes = [];
      const newEdges = [];
      newState.nodes.forEach((e, i) => {
        if (i < idx) {
          newNodes.push(e);
        }
        if (i > idx) {
          newNodes.push({ ...e, id: e.id - 1 });
        }
      });
      newState.edges.forEach((e, i) => {
        //console.log('edge', e);
        if (e.source == idx || e.target == idx) {
          return;
        }
        //console.log('active ^');
        if (e.source > idx) {
          e.source = e.source - 1;
        }
        if (e.target > idx) {
          e.target = e.target - 1;
        }
        newEdges.push(e);
      });
      newState.nodes = newNodes;
      newState.edges = newEdges;
      //console.log('fini state', newState);
      return newState;
    });
  }, [graph, setGraph]);
  const onDeleteSelected = useCallback((e) => {
    //console.log('delete', e);
    if (e.nodes && e.nodes.size == 1) {
      const node = e.nodes.get(Array.from(e.nodes.keys())[0]);
      removeNode(node);
      //console.log('delete', node);
    }   
    else if (e.edges && e.edges.size == 1) {
      const edge = e.edges.get(Array.from(e.edges.keys())[0]);
      removeEdge(edge);
      //console.log('delete', edge);
    }
  }, [removeNode, removeEdge]);
  const updateNode = useCallback((newNode) => {
    setGraph(state => {
      const newState = newValidated(state);
      const idx = newState.nodes.findIndex(e => e._id == newNode._id);
      if (idx != -1) {
        newState.nodes[idx] = newNode;
      }
      return newState;
    });
  }, [setGraph])
  const onCreateEdge = useCallback((source, target) => {
    setGraph(state => {
      const newState = newValidated(state);
      const [ip1, ip2] = getIP(2);
      if (source.type == 'pc' && target.type == 'pc') {
        reserveIP([ip1, ip2]);
      } 
      else if (source.type == 'pc' || target.type == 'pc') {
        reserveIP([ip1]);
      }
      const newEdge = {
        source: source.id,
        target: target.id,
        type: 'emptyEdge',
        sourceIP: source.type == 'pc' ? ip1 : '',
        targetIP: target.type == 'pc' ? source.type == 'pc' ? ip2 : ip1 : '',
        handleText: source.type == 'pc' ? 
          target.type == 'pc' ? `from ${ip1} to ${ip2}` : `${ip1}` : 
          target.type == 'pc' ? `${ip1}` : ``,
      };
      newState.edges = [
        ...newState.edges,
        newEdge,
      ]
      return newState;
    });
  }, [graph, setGraph, IPPool, getIP, reserveIP]);

  const graphRef = useRef(null);
  //console.log('sample dump', sampleDump);

  const onSelect = useCallback((data) => {
    setSelected(data);
    if (data.nodes && data.nodes.size == 1) {
      const node = data.nodes.get(Array.from(data.nodes.keys())[0]);
      //console.log('select', node);
      setState(state => ({...state, selected: node }))
    }
    else {
      setState(state => ({...state, selected: null}));
    }
  }, []);

  const { selected } = state;

  const [pcapData, setPcapData] = useState(null);
  const openPcap = useCallback(async (name) => {
    const base = dev ? process.env.NEXT_PUBLIC_EMULATION_API : process.env.NEXT_PUBLIC_EMULATION_API_PROD;
    const url = new URL(`/pcap/parsed?root=${resp.data.root}&device=${name.replace('.pcap', '')}`, base);
    const data = await axios.get(url.href).then(resp => resp.data);
    setPcapData(data.data);
  }, [resp, setPcapData]);

  const [arpData, setArpData] = useState(null);
  const openArp = useCallback(async (name) => {
    const base = dev ? process.env.NEXT_PUBLIC_EMULATION_API : process.env.NEXT_PUBLIC_EMULATION_API_PROD;
    const url = new URL(`/pcap/arp?root=${resp.data.root}&device=${name.replace('.txt', '')}`, base);
    const data = await axios.get(url.href).then(resp => resp.data);
    setArpData({data: data.data, key: Math.random()});
  }, [resp, setArpData]);

  const reset = useCallback(() => {
    setStage(0);
    setResp(null);
    setPcapData(null);
    setArpData(null);
    rerender();
  }, []);
  
  const value = {
    ...state,
    rawSelected,
    stage,
    reset,
    launch,
    onSelect: stage == 0 || stage == 3 ? onSelect : nop,
    graphRef,
    graph: stage == 0 ? graph : { ...graph, nodes: graph.nodes.concat(animationNodes) },

    IPPool,

    options, setOptions,
    stopAnimation,
    resp,
    openPcap, pcapData,
    openArp, arpData,

    addNode,
    //removeNode,
    onDeleteSelected,
    updateNode,

    keyPrefix,
    onCreateEdge,
  };
  return (
    <GraphContext.Provider value={value}>
      {children}
      {/*SelectedCard selected={selected} graphRef={graphRef}/>*/}
      {/*<Pcap data={sampleDump}/>*/}
    </GraphContext.Provider>
  );
}

export function useContext() {
  const state = React.useContext(GraphContext);
  if (state === undefined) {
    throw new Error('useContext must be used within ContextProvider');
  }
  return state;
}

export function withContext(config = {}) {
  return function(Component) {
    return function GraphPage(props) { 
      return (
        <Provider {...config}>
          <Component {...props}/>
        </Provider>
      );
    }
  }
}
