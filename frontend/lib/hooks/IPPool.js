import React, { useRef, useState, useCallback, useEffect, useMemo, createContext } from 'react';

//TODO rewrite with Set, add IP removal
export default function useIPPool({ defaultBase = '192.168.1.0', initGraph }) {
  const prefix = useMemo(() => defaultBase.split('.').slice(0, 3).join('.'), [defaultBase]);
  const [IPPool, setIPPool] = useState(initGraph.edges.reduce((res, e) => ({ ...res, [e.sourceIP]: true, [e.targetIP]: true }), {}));
  const getIP = useCallback((num = 1) => {
    let max = 0;
    for (const ip of Object.keys(IPPool)) {
      if (ip.startsWith(prefix)) {
        const n = parseInt(ip.split('.')[3]);
        if (n > max) {
          max = n;
        }
      }
    }
    return num > 1 ? Array.from({ length: num }, (_, i) => `${prefix}.${max + 1 + i}`) : `${prefix}.${max + 1}`;
  });
  const reserveIP = useCallback((vals = []) => {
    const valsArray = Array.isArray(vals) ? vals : [vals];
    setIPPool(state => ({ ...state, ...(vals.reduce((res, e) => ({ ...res, [e]: true }), [])) }));
  }, [IPPool, setIPPool]);
  return {
    IPPool,
    getIP,
    reserveIP,
  };
}
