const sample = {
  nodes: [
    {
      id: 0,
      _id: '1',
      title: 'Node A',
      x: 258.3976135253906,
      y: 331.9783248901367,
      type: 'pc',
      applications: [
        {
          type: 'ping',
          dst: '192.168.1.2',
        },
        {
          type: 'sink',
          dst: '192.168.1.2',
        },
      ],
    },
    {
      id: 1,
      _id: '2',
      title: 'Node B',
      x: 593.9393920898438,
      y: 260.6060791015625,
      type: 'pc',
      applications: [
        /*{
          type: 'ping',
          dst: '192.168.1.2',
        },*/
        {
          type: 'sink',
          dst: '192.168.1.2',
        },
      ],
    },
    {
      id: 2,
      _id: '3',
      title: 'Node C',
      x: 600,
      y: 600,
      type: 'switch',
      applications: [],
    },
  ],
  edges: [
    {
      source: 0,
      sourceIP: '192.168.1.1',
      targetIP: '',
      target: 2,
      type: 'emptyEdge',
      handleText: '192.168.1.1',
    },
    {
      source: 1,
      sourceIP: '192.168.1.2',
      targetIP: '',
      target: 2,
      type: 'emptyEdge',
      handleText: '192.168.1.2',
    },
  ]
}

export default sample;
