#!/bin/bash

cd frontend
pm2 start ecosystem.config.js
cd ..
cd backend
nodemon node bin/www
